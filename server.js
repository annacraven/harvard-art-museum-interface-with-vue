const express = require('express');
const proxy = require('express-http-proxy');
const app = express();
const port = 3000
const fetch = require('cross-fetch');
const cors = require('cors');
app.use(cors())
app.use('/vue', proxy('http://localhost:8080'));

app.listen(port, () => console.log(`Example app running on port ${port}`));

// Middleware function to add api key to query
function addKey(req, res, next) {
	req.query.apikey = '7a596d40-ce74-11e9-b536-e54a28d43282'; 
  next();
};

//------------------------------------------------------------------//
// Passport Authentication
// Adapted from Shea Ketsdever
let passport = require('passport');
let GitHubStrategy = require('passport-github').Strategy;
let { URL } = require('url');

// 1. PRIVATE KEYS
const GITHUB_CLIENT_ID = "320e75f653abcc593596";
const GITHUB_CLIENT_SECRET = "59e2b81e76bce10ff1a8ad449c13d702c9460de0";

// This is how Passport keeps track of user sessions / if a login has been successful.
app.use(require('express-session')({ secret: '1Q2W3E4R5T6Y', resave: true, saveUninitialized: true }));

// 3. PASSPORT
// Create github strategy
passport.use(new GitHubStrategy({
  clientID: GITHUB_CLIENT_ID,
  clientSecret: GITHUB_CLIENT_SECRET,
  callbackURL: "http://localhost:3000/auth/github/callback"  // update URL if you don't use localhost
},
function(accessToken, refreshToken, profile, cb) {
  return cb(null, profile);
}
));

// Configure authenticated session persistence. 
passport.serializeUser(function(user, cb) {
  cb(null, user);
});
passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});

// Initialize and restore authentication state, if any, from the session. This will create a
// new session every time the server is restarted -- so note that you have to restart the
// server for it to detect logouts on an external website (eg. github).µµ
//
// These two lines must come AFTER your call to require('express-session')
app.use(passport.initialize());
app.use(passport.session());


// Set up the login callbacks.
app.get('/login',
  passport.authenticate('github'));

app.get('/auth/github/callback', 
  passport.authenticate('github', { failureRedirect: '/login' }),
  function(req, res) {
  	// create a session variable that will keep track of user's searches
  	req.session.searches = {};
    console.log("Successful authentication, redirect home.")
    res.redirect('/');
  });

// End Passport Authentication
//------------------------------------------------------------------//

// Serve up the original html and js files.
app.get('/',
  require('connect-ensure-login').ensureLoggedIn(), // this is how you require authentication on a route
  (req, res) => {
    res.sendFile(__dirname + '/index/index.html')
  });

app.get('/index.js',
  require('connect-ensure-login').ensureLoggedIn(), // this is how you require authentication on a route
  (req, res) => {
    res.sendFile(__dirname + '/index/index.js')
  });


// get all the galleries
app.get('/gallery',
				// require('connect-ensure-login').ensureLoggedIn(), 
				addKey, 
				async function (req, res) {
					let url = create_search_url('gallery', req.query);
					let response = await fetch(url);
					let myJson = await response.json();
				  res.send(stripAPIKey(myJson));
});

// get all the objects for a given gallery
app.get('/gallery/:id/objects',
			  // require('connect-ensure-login').ensureLoggedIn(), 
			  addKey, 
			  async function (req, res) {
					req.query.gallery = req.params['id'];
					let url = create_search_url("object", req.query);
					let response = await fetch(url);
					let myJson = await response.json();
				  res.send(stripAPIKey(myJson));
});

// get the details for a given object id
app.get('/details/:resource/:id',
				// require('connect-ensure-login').ensureLoggedIn(), 
				addKey, 
				async function (req, res) {
					let url = create_details_url(req.params['resource'], req.params['id'], req.query);
					let response = await fetch(url);
					let myJson = await response.json();
				  res.send(myJson);
});


// search for a resource
app.get('/search/:resource', 
				// require('connect-ensure-login').ensureLoggedIn(), 
				addKey, 
				async function (req, res) {
					// do search
					let url = create_search_url(req.params['resource'], req.query);
					let response = await fetch(url);
					let myJson = await response.json();
					// add this search to user session
					delete req.query.apikey;
					let search = {'search': req.params['resource'], 'query': req.query};
					console.log(search);
					// req.session.searches[currentDateTime()] = search;
					// send results of search
				  res.send(stripAPIKey(myJson));
});

// view session search history
app.get('/history', 
				// require('connect-ensure-login').ensureLoggedIn(), 
				addKey, 
				async function (req, res) {
					res.send(req.session.searches);
});


// returns dateTime variable in form 2018-8-3 11:12:40
// from https://tecadmin.net/get-current-date-time-javascript/
function currentDateTime() {
	const today = new Date();
	const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	const dateTime = date+' '+time;
	return dateTime;
}


/* TODO: I need to figure out how to deal with query vs param distinction in 
a cleaner way.
Also I need to allow multiple q=field:value */


// for resource?param=value requests to API
function create_search_url(resource, params) {
	let base_url = "https://api.harvardartmuseums.org/";
	let url = new URL(base_url + resource + "?");
	Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
	return url;
}

// for resource/:resource_id requests to API
function create_details_url(resource, resource_id, params) {
	let base_url = "https://api.harvardartmuseums.org/";
	let url = new URL(base_url + resource + "/" + resource_id + '?');
	Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
	return url;
}

// @param resultsJson [JSON] an object returned from HAM API 
// strips the API key from the next and prev urls returned from HAM API
function stripAPIKey(resultsJson) {
	let patt = /apikey=[^&]+/
	let HAM_url = "https://api.harvardartmuseums.org"
	if (resultsJson.info == undefined) { return resultsJson}
	if (resultsJson.info.next)
	{
		// strip apikey from next, and modify it so /search/:resource can use it
		resultsJson.info.next = resultsJson.info.next.replace(patt, '')
		resultsJson.info.next = resultsJson.info.next.replace('&&', '&')
		resultsJson.info.next = resultsJson.info.next.replace(HAM_url, 'http://localhost:3000/search')
	}
	
	if (resultsJson.info.prev)
	{
		// strip apikey from prev, and modify it so /search/:resource can use it
		resultsJson.info.prev = resultsJson.info.prev.replace(patt, '')
		resultsJson.info.prev = resultsJson.info.prev.replace('&&', '&')
		resultsJson.info.prev = resultsJson.info.prev.replace(HAM_url, 'http://localhost:3000/search')
	}
	
	return resultsJson;
}

app.listen(3000, () => {
  console.log('Express running on localhost:3000');
});
