// basically an enum for the navbar to use to tell which page to show
export const pages = {
	SEARCH: 1,
	SEARCH_HISTORY: 2,
	GALLERIES: 3,
	SEARCH_OBJECT: 4,
	SEARCH_PEOPLE: 5,
	SEARCH_EXHIBITIONS: 6,
	SEARCH_PUBLICATIONS: 7
}